import abc
#
# Base class for a prisoner
#
class PrisonerBase(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self,name):
        pass

    def getName(self):
        return self.name

    @abc.abstractmethod
    def choose(self):
        pass

    @abc.abstractmethod
    def receive_sentence(self, score, others_choice):
        pass

#
# The Judge!
#
class Judge(object):

    def __init__(self):
        self.prisoner_1_cumulative_score = 0
        self.prisoner_2_cumulative_score = 0



    def deliver_verdict(self, prisoner_1, prisoner_2):
        p1_choice = prisoner_1.choose()
        p2_choice = prisoner_2.choose()

        p1_score = p2_score = 0

        if (p1_choice == "cooperate") and (p2_choice == "cooperate"):
            p1_score = p2_score = 3
        elif (p1_choice == "cooperate") and (p2_choice == "defect"):
            p1_score = 0
            p2_score = 5
        elif (p1_choice == "defect") and (p2_choice == "cooperate"):
            p1_score = 5
            p2_score = 0
        else: #both defect
            p1_score = p2_score = 1

        print "Results: Prisoner 1: " + p1_choice + "  Prisoner 2: " + p2_choice
        prisoner_1.receive_sentence(p1_score, p2_choice)
        prisoner_2.receive_sentence(p2_score, p1_choice)

        self.prisoner_1_cumulative_score += p1_score
        self.prisoner_2_cumulative_score += p2_score

    def deliver_final_results(self):
        print "Final Results: Prisoner1: " + str(self.prisoner_1_cumulative_score) + " pts, Prisoner2: " + str(self.prisoner_2_cumulative_score) + " pts"





