from prisoners import *
from sklearn import datasets, linear_model



#####################
#   Persons implementation
#
#
#
class LinearRegression(PrisonerBase):

    def __init__(self,name):
        self.iteration = 1
        self.sample = []
        self.verdict = [] #0 = defect, 1 = cooperate
        self.sample.append(1) # initial value
        self.verdict.append(1)

    def receive_sentence(self, score, others_choice):
        if others_choice == "defect":
            verdict = 0
        else:
            verdict = 1
        self.sample.append(self.iteration)
        self.verdict.append(verdict)


    def choose(self):
        model = linear_model.LinearRegression(True)
        print "Sample " + str(self.sample)
        print "Verdict " + str(self.verdict)
        model.fit(self.sample,self.verdict)
        self.iteration += 1
        choice = model.predict(self.iteration)

        if choice == 0:
            return "defect"
        else:
            return "cooperate"







class AlwaysDefect(PrisonerBase):

    def __init__(self,name):
        self.choice = ""
        self.opponents_last_choice = ""
        self.name = name


    def choose(self):
        self.choice = "defect"
        return self.choice

    def receive_sentence(self, score, others_choice):
        pass
#
#
#
class TitForTat(PrisonerBase):

    def __init__(self,name):
        self.name = name
        self.score = 0
        self.opponents_last_choice = "cooperate" #I want to cooperate on the first move


    def choose(self):
        if self.opponents_last_choice == "defect":
            self.choice = "defect"
        else:
            self.choice = "cooperate"
        return self.choice

    def receive_sentence(self, score, others_choice):
        self.score += score
        self.opponents_last_choice = others_choice
#
#
#
class DoTheOpposite(PrisonerBase):

    def __init__(self,name):
        self.name = name
        self.choice = "defect"

    def choose(self):
        if self.choice == "defect":
            self.choice = "cooperate"
        else:
            self.choice = "defect"
        return self.choice

    def receive_sentence(self, score, others_choice):
        pass

########################################
if __name__ == "__main__":
    P1 = AlwaysDefect("Always-defect")
    P2 = LinearRegression("Linear")

    hanging_Max = Judge()
    for foo in range (1,1000):
        hanging_Max.deliver_verdict(P1, P2)

    print "Prisoner1 type: " + P1.getName() + " Prisoner2 type: " + P2.getName()
    #print "Final Results: Prisoner1: " + str(P1.get_score()) + " pts, Prisoner2: " + str(P2.get_score()) + " pts"
    hanging_Max.deliver_final_results()