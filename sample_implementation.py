from prisoners import *



#####################
#   Persons implementation
#
#
#
class AlwaysDefect(PrisonerBase):

    def __init__(self,name):
        self.name = name

    def choose(self):
        self.choice = "defect"
        return self.choice

    def receive_sentence(self, score, others_choice):
        pass



########################################
if __name__ == "__main__":
    P1 = AlwaysDefect("Defector #1")
    P2 = AlwaysDefect("Defector #2")

    hanging_Max = Judge()
    for foo in range (1,1000):
        hanging_Max.deliver_verdict(P1, P2)

    print "Prisoner1 type: " + P1.getName() + " Prisoner2 type: " + P2.getName()
    #print "Final Results: Prisoner1: " + str(P1.get_score()) + " pts, Prisoner2: " + str(P2.get_score()) + " pts"
    hanging_Max.deliver_final_results()